# Deblur Music Sheet Images

This project focuses on developing a deep learning model to deblur images of music sheets. Utilizing Convolutional Neural Networks (CNNs), the model aims to enhance the readability of blurred music notations, facilitating easier interpretation and analysis.

![Example of model performance](example.png)

## Requirements

To run the scripts in this project, you need Python version 3.11 or newer (but not newer than 3.13). Additional dependencies include:

- JupyterLab (^4.0.7)
- Torchvision (^0.16.0+cu118) - Please ensure to install a version compatible with your CUDA version.
- Numpy (^1.26.1)
- Matplotlib (^3.8.0)
- JupyterNotify (^0.1.15)
- Pillow (^10.1.0)
- PyPDF2 (^3.0.1)
- PDF2Image (^1.16.3)

You can install these dependencies using `pip`. Here's an example command to install Numpy:
```bash
pip install numpy==1.26.1
```
Repeat the installation for each dependency, adjusting the package name and version accordingly.

## Dataset
The dataset used in this project comprises pairs of segmented music sheet images: original and Gaussian blurred versions. A dataset is provided in the zip file on [Kaggle](https://www.kaggle.com/datasets/lachimolalala/blurred-music-sheets), or you can create your own using the provided scripts.

### Creating Your Dataset
1. Split PDF Files: First, select a PDF file of music sheets and use the pdfSplitter.py script to split it into individual images.
```bash
python3 pdfSplitter.py dir music_piece.pdf start_page end_page
```
2. Blur Images: Next, use the imgBlur.py script to apply Gaussian blur to the split images.
```bash
python imgBlur.py original_img blurred_img
```
## Usage
To test the deblurring model with your images, use the unblurr.py script:
```bash
python3 unblurr.py pathToImg.img
```
This script will process the specified image and output a deblurred version, leveraging the trained CNN model to enhance image clarity.





