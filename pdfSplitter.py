import os
from pdf2image import convert_from_path
from PIL import Image
import sys

def split_pdf_pages(pdf_path, output_folder, x_pieces, y_pieces, start_page, end_page, exclude_top_percent=0.15):
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    # Convert PDF pages to images
    pages = convert_from_path(pdf_path, 250, first_page=start_page, last_page=end_page)


    for page_num, page_image in enumerate(pages):
        page_num += start_page
        img_path = f'{output_folder}/page_{page_num}.jpg'
        page_image.save(img_path, 'JPEG')

        img = Image.open(img_path)
        width, height = img.size
        start_height = int(height * exclude_top_percent)
        piece_height  = (height - start_height) // y_pieces
        piece_width = width // x_pieces

        for i in range(y_pieces):
          for j in range(x_pieces):
              left = j * piece_width
              top = start_height + i * piece_height
              right = (j + 1) * piece_width if j < x_pieces - 1 else width
              bottom = (i + 1) * piece_height + start_height if i < y_pieces - 1 else height

              img_piece = img.crop((left, top, right, bottom))
              filename = pdf_path.split(".")[0]
              img_piece.save(f'{output_folder}/{filename}_page_{page_num}_piece_{i}_{j}.jpg')


        os.remove(img_path) 

# Example usage
#split_pdf_pages('test.pdf', 'imgs', 2, 4)  # Adjust parameters as needed
if __name__ == "__main__":
    
    if len(sys.argv) < 5:
        print("Usage: python cropScript.py <folderName> <filename> <start_page> <end_page>")
    else:
      folderName = sys.argv[1]
      pdf_file = sys.argv[2]
      start_page = int(sys.argv[3])
      end_page = int(sys.argv[4])

      while start_page <= end_page:
          current_end_page = min(start_page + 99, end_page)  # Process up to the next 100 pages
          split_pdf_pages(pdf_file, folderName, 5, 7, start_page, current_end_page)  # Adjust other parameters as needed
          print("wrote pages:", start_page, "-", current_end_page)
          start_page = current_end_page + 1
      print("Done!")