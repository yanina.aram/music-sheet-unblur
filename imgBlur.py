import os
from PIL import Image, ImageFilter
import random
import sys
import time
def blur_images(input_folder, output_folder, blur_min, blur_max):
    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    for filename in os.listdir(input_folder):
      if filename.endswith('.jpg') or filename.endswith('.png'):  
          img_path = os.path.join(input_folder, filename)
          img = Image.open(img_path)

          # Apply a random Gaussian blur
          blur_amount = random.randint(blur_min, blur_max)
          blurred_img = img.filter(ImageFilter.GaussianBlur(blur_amount))

     

          blurred_img.save(os.path.join(output_folder, filename))

# Example usage
#blur_images('imgs', 'blurImgs', 6, 13)  
#blur_images('smallData', 'smallDataBlur', 13, 17) 
#blur_images('smallD', 'smallBlurD', 6, 16)  

if __name__ == "__main__":

    if len(sys.argv) < 3:
        print("Usage: python imgBlur.py <FolderNameFrom> <FolderNameTo>")
    else:
        folderNameFrom = sys.argv[1]
        folderNameTo = sys.argv[2]
        start_time = time.time()  
        blur_images(folderNameFrom, folderNameTo, 6, 16)  
        end_time = time.time() 
  
        print(f"Done! Time taken: {(end_time - start_time)/60:.2f} minutes")
