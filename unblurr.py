import torch
from torchvision import transforms
import torch.nn.functional as F
import torch.nn as nn
from PIL import Image, ImageOps
import sys
import time
import os
from torchvision.utils import save_image
import numpy as np
import matplotlib.pyplot as plt

modelName = 'model.pth' 
t_size = 256
device = 'cuda:0' if torch.cuda.is_available() else 'cpu'

class DeblurCNN(nn.Module):
  def __init__(self, dropout_rate=0.2):
    super(DeblurCNN, self).__init__()
    self.conv1 = nn.Conv2d(1, 100, kernel_size=13, padding=5)
    self.conv2 = nn.Conv2d(100, 64, kernel_size=7, padding=2)
    self.conv3 = nn.Conv2d(64, 32, kernel_size=1, padding=2)
    self.conv4 = nn.Conv2d(32, 1, kernel_size=5, padding=2)
    self.dropout = nn.Dropout(dropout_rate)

  def forward(self, x):
    x = F.relu(self.conv1(x))
    x = self.dropout(x)
    x = F.relu(self.conv2(x))
    x = self.dropout(x)
    x = F.relu(self.conv3(x))
    x = self.dropout(x)
    x = self.conv4(x)
    return x


def getName(img_path):
    directory = os.path.dirname(img_path)  
    base_name = os.path.basename(img_path)  
    img_name, _ = os.path.splitext(base_name)  
    new_name = img_name + "_unblurred.jpg" 

    return new_name if directory == '' else os.path.join(directory, new_name)

def stitch_images(numpy_arrs, x_pieces=5, y_pieces=8):

    index = 0
    stitched_array = np.zeros((y_pieces*t_size, x_pieces*t_size))

    for i in range(y_pieces):
      for j in range(x_pieces):
        left = j * t_size
        top = i * t_size
        right = (j + 1) * t_size if j < t_size - 1 else x_pieces*t_size
        bottom = (i + 1) * t_size  if i < t_size - 1 else y_pieces*t_size
        numpy_arr = numpy_arrs[index]

        stitched_array[top:bottom, left:right] = numpy_arr
        index += 1
    
    threshold = 210  # Pixel values below this are considered notes
    scaling_factor = 0.56
    stitched_array = (stitched_array * 0.5 + 0.5) * 255  # Undo normalization
    darker_notes = np.where(stitched_array < threshold, stitched_array * scaling_factor, stitched_array)
    darker_notes = darker_notes.clip(0, 255).astype('uint8')  # Clip values and convert back to uint8

    return darker_notes

        


def preprocess_and_unblur(image):
  """Preprocess the image, unblur the pieces, and stitch them back together."""
  images = split_img(image)
  transform = transforms.Compose([
          transforms.Resize((256, 256)),
          transforms.ToTensor(),
          #transforms.Normalize((0.5,), (0.5,))
      ])
 
  processed_images = []
  for img in images:
    input_tensor = transform(img).unsqueeze(0).to(device)

    with torch.no_grad():
      output_tensor = model(input_tensor)

    deblurred_image = output_tensor.squeeze().cpu().numpy()
    processed_images.append(deblurred_image)
    
  return stitch_images(processed_images)



def loadModel():
  print("device:", device)
  model = DeblurCNN().to(device)
  model.load_state_dict(torch.load(modelName, map_location=torch.device(device)))
  model.eval()
  return model



def split_img(img,  x_pieces=5, y_pieces=8):
  width, height = img.size
  piece_height  = height  // y_pieces
  piece_width = width // x_pieces
  crops = []
  for i in range(y_pieces):
    for j in range(x_pieces):
      left = j * piece_width
      top = i * piece_height
      right = (j + 1) * piece_width if j < x_pieces - 1 else width
      bottom = (i + 1) * piece_height  if i < y_pieces - 1 else height

      img_piece = img.crop((left, top, right, bottom))
      crops.append(img_piece)

  return crops 

if __name__ == "__main__":

    if len(sys.argv) < 2:
        print("Usage: python3 unblurr.py <pathToImg> ")
    else:
        start_time = time.time()  # Start timing
        image_path = sys.argv[1]
        model = loadModel()
        image = Image.open(image_path).convert('L')
        output_image = preprocess_and_unblur(image)

        image_res = Image.fromarray(output_image).resize(image.size)

        image_res.save(getName(image_path), "JPEG")

        end_time = time.time()  # End timing
  
        print(f"Done! Time taken: {(end_time - start_time):.2f} seconds")

